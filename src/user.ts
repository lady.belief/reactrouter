export interface User {
    userName: string;
    password: string;
    isAutorized: boolean;
}