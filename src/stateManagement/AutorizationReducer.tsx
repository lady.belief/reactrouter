import { User } from "../user";

export const LoginState = Object.freeze({
    LOGIN_CHECK: '[LOGIN_STATE] LOGIN_CHECK',
});

export const Actions = Object.freeze({
    checkUser: (user: User) => ({type: LoginState.LOGIN_CHECK, payload: user })
});

interface State {
    user: User;
}

const initialState: State = {
    user: { userName: "", password: "", isAutorized: false}
}

export const AutorizationReducer = (state = initialState, action: any) => {
    switch(action.type) {
        case LoginState.LOGIN_CHECK:
            const user = { userName: action.payload.userName, password: action.payload.password, isAutorized: false };
            if(action.payload.userName === "" || action.payload.password === "") {
                user.isAutorized = false;
            }
            else {
                user.isAutorized = true;
            }
            console.log("AutorizationReducer: user = ", user);
            return {...state, user};
    
        default:
            return state;
    }
}

export default AutorizationReducer;